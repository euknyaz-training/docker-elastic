# docker-elastic - Docker-compose environment for Elasticsearch and Kibaba

# Install

Installation commands:
```
# step 1
git clone git@gitlab.com:euknyaz-training/docker-elastic.git
cd docker-elastic

# step 2
docker-compose up -d

sleep 30 # wait for all processes to start

# step 3 - verify elasticsearch port
curl http://localhost:9200
```

# Access

* Elasticsearch URL: http://localhost:9200/
* Kibana URL: http://localhost:5601/ 

# Docker commands

```
docker-compose up -d
docker-compose down
docker ps | grep de
```

# Import Data and Kibana Dashboards 

* Import Kibana Sample Dashboards with Sample Data - http://localhost:5601/app/kibana#/home/tutorial_directory/sampleData?_g=()


# Import CSV Data with Kibana

* Importing CSV and Log Data into Elasticsearch with File Data Visualizer - https://www.elastic.co/blog/importing-csv-and-log-data-into-elasticsearch-with-file-data-visualizer
* Use the following CSV Import wizard: http://localhost:5601/app/ml#/filedatavisualizer?_g=()


# Troubleshooting

* In case of high disk usage Elasticsearch's index is made read-only and never set back when disk watermark reached high.
To resolve the issue execute the following commands:

```
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_cluster/settings -d '{ "transient": { "cluster.routing.allocation.disk.threshold_enabled": false } }'
curl -XPUT -H "Content-Type: application/json" http://localhost:9200/_all/_settings -d '{"index.blocks.read_only_allow_delete": null}'
```
